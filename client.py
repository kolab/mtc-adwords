#-*- coding: utf-8 -*-
import base64, codecs, csv
from datetime import datetime, timedelta

import jinja2
from decouple import config, Csv

from sparkpost import SparkPost
from googleads import adwords, oauth2

API_VERSION = config('API_VERSION')
ACCOUNT_ID = config('ACCOUNT_ID')
MANDRILL_KEY = config('MANDRILL_KEY')
SPARKPOST_KEY = config('SPARKPOST_KEY')
RECEIVERS = config('RECEIVERS', cast=Csv())
STATIC_URL = config('STATIC_URL', default='')

# Google Authentication Parameters
CLIENT_ID = config('CLIENT_ID')
CLIENT_SECRET = config('CLIENT_SECRET')
REFRESH_TOKEN = config('REFRESH_TOKEN')
DEVELOPER_TOKEN = config('DEVELOPER_TOKEN')
USER_AGENT = config('USER_AGENT')
CLIENT_CUSTOMER_ID = config('CLIENT_CUSTOMER_ID')

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'))


class Radar(object):
    def __init__(self, account_id):
        oauth2_client = oauth2.GoogleRefreshTokenClient(
            CLIENT_ID, CLIENT_SECRET, REFRESH_TOKEN
        )

        self.client= adwords.AdWordsClient(
            DEVELOPER_TOKEN, oauth2_client, USER_AGENT, CLIENT_CUSTOMER_ID
        )

        self.client.SetClientCustomerId(account_id)

    def get_last_90_days_average_search_by_category(self):
        ninety_days_ago = datetime.today() - timedelta(days=90)
        ninety_days_ago_str = datetime.strftime(ninety_days_ago, '%Y%m%d')
        today_str = datetime.strftime(datetime.today(), '%Y%m%d')

        report = {
            'reportName': 'Last 90 Days Performance Report',
            'dateRangeType': 'CUSTOM_DATE',
            'reportType': 'ADGROUP_PERFORMANCE_REPORT',
            'downloadFormat': 'CSV',
            'selector': {
                'fields': [
                    'AdGroupName', 'Impressions', 'SearchImpressionShare',
                    'Labels',
                ],
                'dateRange': {'min': ninety_days_ago_str, 'max': today_str},
            },
            'includeZeroImpressions': 'false'
        }

        report_downloader = self.client.GetReportDownloader(version=API_VERSION)

        stream_data = report_downloader.DownloadReportAsStream(report)
        stream_data.readline()
        reader = csv.DictReader(codecs.iterdecode(stream_data, encoding='utf-8'))

        search_count = {}
        data = [x for x in reader]
        for x in data[:-1]:
            try:
                search_count[x['Ad group']] = ((int(x['Impressions'])*100)/float(x['Search Impr. share'][:-1]))/ 90
            except:
                continue

        return search_count

    def get_last_7_days_average_search_by_category(self):
        report = {
            'reportName': 'Last 7 Days Performance Report',
            'dateRangeType': 'LAST_7_DAYS',
            'reportType': 'ADGROUP_PERFORMANCE_REPORT',
            'downloadFormat': 'CSV',
            'selector': {
                'fields': [
                    'AdGroupName', 'Impressions', 'SearchImpressionShare',
                    'Labels',
                ],
            },
            'includeZeroImpressions': 'false'
        }

        report_downloader = self.client.GetReportDownloader(version=API_VERSION)

        stream_data = report_downloader.DownloadReportAsStream(report)
        stream_data.readline()
        reader = csv.DictReader(codecs.iterdecode(stream_data, encoding='utf-8'))

        search_count = {}
        data = [x for x in reader]
        for x in data[:-1]:
            try:
                search_count[x['Ad group']] = ((int(x['Impressions'])*100)/float(x['Search Impr. share'][:-1]))/ 7
            except:
                continue

        return search_count

    def get_last_share_by_group(self):
        three_days_ago = datetime.today() - timedelta(days=3)
        three_days_ago_str = datetime.strftime(three_days_ago, '%Y%m%d')

        report = {
            'reportName': '3 Days Ago Performance Report',
            'dateRangeType': 'CUSTOM_DATE',
            'reportType': 'ADGROUP_PERFORMANCE_REPORT',
            'downloadFormat': 'CSV',
            'selector': {
                'fields': [
                    'AdGroupName', 'SearchImpressionShare',
                ],
                'dateRange': {'min': three_days_ago_str, 'max': three_days_ago_str},
            },
            'includeZeroImpressions': 'false'
        }

        report_downloader = self.client.GetReportDownloader(version=API_VERSION)

        stream_data = report_downloader.DownloadReportAsStream(report)
        stream_data.readline()
        reader = csv.DictReader(codecs.iterdecode(stream_data, encoding='utf-8'))

        last_share = {}
        data = [x for x in reader]
        for x in data[:-1]:
            last_share[x['Ad group']] = float(x['Search Impr. share'][:-1])

        return last_share

    def today_report(self):
        report = {
            'reportName': 'Today Performance Report',
            'dateRangeType': 'TODAY',
            'reportType': 'ADGROUP_PERFORMANCE_REPORT',
            'downloadFormat': 'CSV',
            'selector': {
                'fields': [
                    'AdGroupName', 'Impressions', 'SearchImpressionShare',
                    'Labels',
                ],
            },
            'includeZeroImpressions': 'false'
        }

        report_downloader = self.client.GetReportDownloader(version=API_VERSION)

        stream_data = report_downloader.DownloadReportAsStream(report)
        stream_data.readline()
        reader = csv.DictReader(codecs.iterdecode(stream_data, encoding='utf-8'))

        return [x for x in reader][:-1]

    def today_searches(self):
        shares = self.get_last_share_by_group()
        today_impressions = self.today_report()

        searches = {}
        for c in today_impressions:
            try:
                searches[c['Ad group']] = ((int(c['Impressions'])*100)/shares[c['Ad group']])
            except:
                continue

        return searches


def create_email(adgroups):
    warning_adgroups = adgroups[0]
    attention_adgroups = adgroups[1]
    txt_template = template_env.get_template('email.txt')
    html_template = template_env.get_template('email-inline.html')
    txt_message = txt_template.render(warning_adgroups=warning_adgroups, attention_adgroups=attention_adgroups)

    html_message = html_template.render(warning_adgroups=warning_adgroups, attention_adgroups=attention_adgroups, static_url=STATIC_URL)

    return txt_message, html_message

def generate_csv(adgroups):
    with open('radar-mtc.csv', 'w') as fd:
        fieldnames = [
            'name', 'status', 'today_searches',
            'last_7_days_average', 'last_90_days_average'
        ]
        writer = csv.DictWriter(fd, fieldnames=fieldnames)
        writer.writeheader()
        for adgroup in adgroups:
            writer.writerow(adgroup)

    with open('radar-mtc.csv', 'r') as fd:
        return fd.read()

def send_email(text, html, csvfile):
    sp = SparkPost(SPARKPOST_KEY)
    encoded = base64.b64encode(bytes(csvfile, 'utf-8')).decode('utf-8')

    response = sp.transmissions.send(
        recipients=RECEIVERS,
        text=text,
        html=html,
        from_email='no-reply@gate.cx',
        subject='Alerta Radar MTC',
        track_opens=True,
        track_clicks=False,attachments=[
            {
                "name": "RadarMTC.csv",
                "type": "text/csv",
                "data": encoded,
            }
        ]
    )

    return response

def list_to_bcc_dict(address_list):
    return list(map(lambda x: {'email': x, 'type': 'bcc'}, address_list))

if __name__ == '__main__':
    radar = Radar(ACCOUNT_ID)
    searches = radar.today_searches()
    ninety_average = radar.get_last_90_days_average_search_by_category()
    seven_average = radar.get_last_7_days_average_search_by_category()

    adgroups = []

    warning_adgroups = []
    attention_adgroups = []

    SEND_MAIL_FLAG = False
    for k, v in searches.items():
        adgroup = {
            'name': k,
            'today_searches': '%.0f' % v,
            'last_7_days_average': '%.0f' % seven_average[k],
            'last_90_days_average': '%.0f' % ninety_average[k],
            'status': 'OK'
        }

        if v > 10:
            if seven_average[k] > v > ninety_average[k]:
                adgroup['status'] = 'ATTENTION'
                attention_adgroups.append(adgroup)
                SEND_MAIL_FLAG = True
            elif v > (seven_average[k] * 1.2):
                adgroup['status'] = 'WARNING'
                warning_adgroups.append(adgroup)
                SEND_MAIL_FLAG = True

        adgroups.append(adgroup)

    if SEND_MAIL_FLAG:
        csvfile = generate_csv(adgroups)
        text, html = create_email((warning_adgroups, attention_adgroups))

        send_email(text, html, csvfile)
